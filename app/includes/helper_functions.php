<?php

namespace WarehousesHelpers;

spl_autoload_register( function( $class ) {

  $parts = explode('\\', $class);
  $file_name = PATH_ROOT . 'classes/' . end($parts) . '.php';

  if ( file_exists( $file_name ) ) {
    require_once( $file_name );
  }

} );


function redirect( $url, $statusCode = 303 )
{
   header( 'Location: ' . $url, true, $statusCode );
   die();
}