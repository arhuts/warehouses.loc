<?php

namespace Warehouses;

class View
{
    const PATH_VIEW_PARTS = PATH_ROOT . 'includes/templates/';
    const TPL_VALUE = '{{ % }}';
    protected $col_names;

    function __construct( $output, $col_names )
    {
        $this->col_names = $col_names;

        $errors = Errors::pull();
        Errors::clear();
        echo $this->get_errors( $errors );

        echo $this->get_template( 'form' );

        echo $this->get_table( $output );
    }


    private function get_table( $data )
    {
        $table_data = '';

        if( is_array( $data ) )
        {
            $template = $this->get_template( 'table_row' );

            foreach($data as $cells)
            {
                $table_data .= $this->parse_template(
                    $template,
                    $this->col_names,
                    $cells
                );
            }
        }

        return $this->parse_template( $this->get_template( 'table' ),
            ['table_data'], $table_data
        );
    }

    protected function parse_template( $template, $values_names, $data )
    {
        $value_names = $this->tpl_symbols( $values_names );
        return str_replace( $value_names, $data, $template );
    }

    protected function tpl_symbols( $array )
    {
        $cb = function( $item )
        {
            return str_replace( '%', $item, self::TPL_VALUE );
        };

        return array_map( $cb, $array );
    }

    protected function get_template( $name )
    {
        ob_start();
        require_once ( self::PATH_VIEW_PARTS . $name . '.tpl' );
        return ob_get_clean();
    }

    protected function get_errors( $list )
    {
        $html = '';

        if( is_array( $list ) && count( $list ) )
        {
            $template = $this->get_template( 'error' );
            foreach( $list as $error )
            {
                $html .= $this->parse_template( $template, ['error'], $error );
            }
        }

        return $html;
    }

}