<?php

namespace Warehouses;

class Storage
{

    const STOR_NAME = 'product_data';

    static function put( $data, $stor_name = self::STOR_NAME )
    {
        $_SESSION[ $stor_name ] = json_encode( $data );
    }

    static function get( $stor_name = self::STOR_NAME )
    {
        return isset( $_SESSION[ $stor_name ] ) ? json_decode( $_SESSION[ $stor_name ], true ) : [];
    }

    static function remove( $stor_name = self::STOR_NAME )
    {
        unset( $_SESSION[ $stor_name ] );
    }

}