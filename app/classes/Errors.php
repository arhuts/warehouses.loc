<?php

namespace Warehouses;

class Errors
{
    const STOR_NAME = 'errors';
    static private $errors_list = [];

    static function add( $error )
    {
        self::$errors_list[] = $error->getMessage();
        self::save();
    }

    static protected function save()
    {
        Storage::put(
            self::$errors_list,
            self::STOR_NAME
        );
    }

    static function pull()
    {
        return Storage::get(
            self::STOR_NAME
        );
    }

    static function clear()
    {
        Storage::remove(
            self::STOR_NAME
        );
    }


}