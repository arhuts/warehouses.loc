<?php 

/*
    It's main class which run the whole app
*/

namespace Warehouses;

use WarehousesHelpers as Helpers;

class Controller
{

    // order of the columns for csv file
    const COLS_ORDER = ['product_name','qty','warehouse'];

    function __construct()
    {
        session_start();
        
        $current_data = Storage::get();
        try
        {
            $file = $this->get_file_data();
            $file_data = $this->parse_file( $file );
        } catch ( \Exception $e ){
            Errors::add( $e );
        }
        
        $data = '';
        if( !empty( $file ) )
        {
            if( !empty( $file_data ) )
            {
                $data = $this->add_new_items_to_data( $current_data, $file_data );
                Storage::put( $data );
            }
            Helpers\redirect( $_SERVER['REQUEST_URI'], false );
        } else {
            $data = $current_data;
        }

        $output = $this->prepare_out_data( $data );

        $View = new View( $output, self::COLS_ORDER );
    }



    private function get_file_data()
    {
        if( !isset( $_POST['upload_file']) || !isset($_FILES['uploaded_csv_file'])) return false;
        
        $file = $_FILES['uploaded_csv_file'];

        if( $file['type'] != 'text/csv' )
        {
            throw new \Exception('Invalid file format.');
        }

        if( $file['error'] != UPLOAD_ERR_OK || !is_uploaded_file( $file['tmp_name'] ) )
        {
            throw new \Exception('Error occurred while uploading the file.');
        }

        $data = file_get_contents( $file['tmp_name'] );
        return $data;
    }

    private function parse_file( $content )
    {
        if( !empty( $content ) )
        {
            // split up file strings to array
            $strings = explode(PHP_EOL, $content);
            
            $cb = function( $item )
            {
                return !empty( $item );
            };

            $strings = array_filter( $strings, $cb );

            $cb = function( $item )
            {
                return str_getcsv( $item );
            };
            $strings = array_map( $cb, $strings );

            if( !$this->is_right_cols_order( $strings[0] ) )
            {
                throw new \Exception('Table columns in file have wrong order.');
            }
            $strings = $this->remove_col_names( $strings );
            // convert the quantity fields to integer
            $strings = $this->convert_qty_fields( $strings );
            return $strings;
        }
    }

    public function is_right_cols_order( $names )
    {
        return self::COLS_ORDER === $names ? true : false;
    }

    public function convert_qty_fields( $array )
    {
        $output = [];
        $count = count( $array );
        for( $i = 0; $i < $count; $i++ )
        {
            $item = $array[ $i ];

            if( is_numeric( $item[1] ) )
            {
                $item[1] = (int) $item[1];
                $output[] = $item;
            } else {
                throw new \Exception('File has a mistake in row - ' . ( $i + 2 ) );
            }
        }
        return $output;
    }

    protected function remove_col_names( $array )
    {
        array_shift( $array );
        return $array;
    }


    private function add_new_items_to_data( $old, $new )
    {        
        $handled_data = $old;

        foreach($new as $row)
        {
            $filter_cb = function( $value ) use ( $row )
            {
                return $value[0] == $row[0] && $value[2] == $row[2];
            };
            
            if( $existent = array_filter( $old, $filter_cb ) )
            {
                $key = array_keys( $existent );
                // add new product amount
                $handled_data[ $key[0] ][ 1 ] += $row[ 1 ];

                // remove a product if the amount is less 0
                if( $handled_data[ $key[0] ][ 1 ] < 1 )
                {
                    unset( $handled_data[ $key[0] ] );
                }
            } 
            // if in new product the quantity is positive then add one to array
            elseif( $row[1] > 0 )
            {
                $handled_data[] = $row;
            }
        }

        return $handled_data;
    }

    protected function prepare_out_data( $table )
    {
        $output = [];

        if( !is_array( $table ) ) return $output;

        // unite the similar products
        foreach( $table as $item )
        {
            $pos = array_search($item[0], array_column($output, 0));
            
            if( $pos !== false )
            {
                $output[ $pos ][1] += $item[1];
                $output[ $pos ][2] .= ', ' . $item[2];
            } else {
                $output[] = $item;
            }
        }
        
        return $output;
    }

}